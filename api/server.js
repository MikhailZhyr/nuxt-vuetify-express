
const express = require('express')
const app = express()


const posts = [
  {id: 1, title: 'Наше дело не так однозначно, как может показаться', role: 'USER', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dignissim luctus ex a mollis. Nam non libero pulvinar, sodales urna sodales, placerat velit. Pellentesque luctus sem eget vehicula commodo. Morbi mattis neque eu orci pharetra rutrum. Phasellus lacus metus, euismod at erat a, pharetra semper dolor. Morbi pellentesque sodales nisi semper tempor. Praesent maximus hendrerit mi eget vestibulum.'},
  {id: 2, title: 'В целом, конечно, курс на социально-ориентированный ', role: 'USER', description: 'In sodales vestibulum sapien, eget pretium augue gravida sed. Suspendisse eget enim hendrerit, pharetra est eu, feugiat arcu. Pellentesque venenatis ac tellus quis mollis. Nullam viverra malesuada nunc in lobortis. Vivamus faucibus vulputate malesuada. Integer vitae leo eros. Pellentesque dapibus tristique libero, vitae lobortis nisl euismod quis. Pellentesque nec luctus lectus. Suspendisse potenti. Sed eu purus in justo convallis vestibulum in at nulla. Maecenas commodo maximus erat, et varius odio venenatis id. Maecenas venenatis mi vel placerat laoreet.'},
  {id: 3, title: 'А также базовые сценарии поведения пользователей', role: 'USER', description: 'Mauris posuere cursus egestas. Duis bibendum rutrum gravida. Ut sit amet purus ac justo tristique bibendum a in est. Vestibulum finibus tempor felis, non consequat eros. Suspendisse in hendrerit nisi. Sed rutrum massa id placerat scelerisque. Cras pharetra magna fringilla est convallis, varius dignissim nibh fermentum. Donec metus lectus, elementum non nibh sit amet, lacinia suscipit elit. Duis a vulputate nisl. Donec sit amet ullamcorper quam, eget aliquam massa. Sed vel venenatis libero. Nullam ut ornare turpis, et maximus sem. Aliquam erat volutpat.'},
  {id: 4, title: 'Для современного мира внедрение современных', role: 'USER', description: 'Morbi et mollis turpis, id venenatis justo. Sed blandit a ante ut eleifend. Suspendisse pharetra viverra dolor fringilla porttitor. Mauris pellentesque, tortor at ultrices facilisis, dui dolor porta metus, non pretium sem eros sed neque. Etiam venenatis elit vitae consectetur consectetur. Nullam egestas libero ac volutpat tincidunt. Nam at tempus lacus. Sed faucibus lacus sem, at ultrices elit euismod at.'},
  {id: 5, title: 'Не следует, однако, забывать, что перспективное', role: 'USER', description: 'Maecenas finibus in lectus a convallis. Suspendisse vel sem dolor. Donec interdum nulla massa, nec efficitur mauris viverra vel. Quisque nec erat erat. Proin vulputate elementum est non fermentum. Aenean cursus, metus a imperdiet ultrices, arcu tortor volutpat neque, nec feugiat tellus sapien a dolor. Nunc egestas metus justo. Cras eleifend sollicitudin nibh, eget tristique arcu tempor auctor. Suspendisse convallis, nunc ut tincidunt porta, turpis mauris bibendum arcu, eget dictum libero nunc ac quam. Nunc vel varius orci.'},
  {id: 6, title: 'Таким образом, реализация намеченных плановых', role: 'USER', description: 'nteger vel pretium dolor, eu auctor tellus. Nullam fringilla ullamcorper eleifend. Sed mollis turpis eu tellus dapibus, eu faucibus felis lobortis. Donec lacinia, velit eget varius dignissim, enim purus auctor mi, quis condimentum nulla tortor et orci. Aliquam sollicitudin erat felis, quis facilisis ligula vestibulum vel. Maecenas eget nisi ac sem tempus finibus a eu nisi. Nam tincidunt ligula neque, a eleifend nisl egestas in. Morbi hendrerit dictum convallis. Praesent viverra lectus eu ultrices tristique. Vestibulum eget ipsum libero. Suspendisse tempor tristique pretium. Vivamus scelerisque nunc non efficitur lobortis. Nunc tempus lorem eget malesuada interdum.'},
  {id: 7, title: 'Как принято считать, независимые государства', role: 'USER', description: 'Curabitur condimentum magna turpis, eu ultricies mauris efficitur vel. Vestibulum accumsan eu turpis id maximus. Pellentesque quis enim fringilla, finibus nisl et, vestibulum tellus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vehicula blandit vulputate. Nunc pulvinar id ex quis viverra. Nam mauris orci, varius in vehicula eu, sagittis id ipsum.'},
  {id: 8, title: 'Как принято считать, сделанные на базе', role: 'ADMIN', description: 'Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque maximus augue et eros condimentum ornare. Vestibulum scelerisque justo sed sollicitudin commodo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras lacus quam, lobortis ultricies faucibus ac, tristique non lacus. Pellentesque sapien sapien, sagittis a elit non, tincidunt consectetur lectus. Phasellus lacinia ipsum lorem, quis euismod magna scelerisque eget. Sed sit amet venenatis dolor.'}
]

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.get('/posts', function (req, res) {
  res.json({
    posts
  })
})
app.get('/posts/:id', function (req, res) {
  const post = posts.find(item => item.id == req.params.id)

  res.json({
    post
  })
})


export default {
  path: '/api',
  handler: app
}
